import {
  IsNotEmpty,
  Length,
  IsInt,
  Min,
  IsString,
  MinLength,
} from 'class-validator';

export class CreateCustomerDto {
  @IsString()
  @MinLength(4)
  @IsNotEmpty()
  name: string;

  @IsInt()
  @Min(0)
  @IsNotEmpty()
  age: number;

  @Length(10)
  @IsNotEmpty()
  tel: string;

  @IsString()
  @IsNotEmpty()
  gender: string;
}
